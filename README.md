# marg

## Description
marg is a simple argument handling library written in C. The process of using the library is the following: define arguments to look for, pass the command-line
for parsing and retrieve parsed arguments.

Both short and long forms of options are supported: options in short form are prefixed with one '-' and are one character long, while options in long form are prefixed
with two '-'s and are two or more characters long. If an option requires a value, next argument on the command-line is taken to be its value.

Only ASCII is supported.

## Requirements
To build and install this library you'll need:

+ make
+ sh
+ coreutils
+ cc
+ ar (for static library)

GCC and Clang are guaranteed to compile the project.

## Installation
Enter the project's root directory and run the following commands:

```
make lib OPTIMIZE=1     # Build optimized library.
make install-lib        # Install library.
make install-headers    # Install headers.
make install-man        # Install manual.
```

By default, library that make builds and installs is dynamic library.
If you want make to build/install static library, set `LIBRARY` make variable to "static".

To see the entire list of variables that control the behavior of the make script run `make help`.

## Uninstallation
Enter the project's root directory and run the following commands:

```
make uninstall-lib      # Uninstall library.
make uninstall-headers  # Uninstall headers.
make uninstall-man      # Uninstall manual.
```

Make sure to set all of the relevant make variables to the values that were used for the installation.
