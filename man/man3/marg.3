.\" Man page for marg
.TH "MARG" 3 "27 January 2021" "0.3" "Man page for marg"
.SH NAME
marg \- simple argument handling library for C programs
.SH SYNOPSIS
.nf
.B #include <marg/marg.h>
.P
.BI "enum MargSuccess marg_init(const unsigned int " "count" ");"
.BI "enum MargSuccess marg_set_arg(const char " "short_form" ", const char * const " "long_form" ", const bool " "takes_a_value" ");"
.BI "enum MargSuccess marg_parse_args(const int * const " "argc" ", const char * const * const " "argv" ", const bool " "extra" ");"
.BI "const struct MargArg *marg_get_arg(const unsigned int " "n" ");"
.B void marg_close();
.fi
.SH DESCRIPTION
marg is a simple argument handling library written in C.
The process of using the library is the following: define the arguments to look for, pass the command-line for parsing and retrieve parsed arguments.
.P
Both short and long forms of options are supported: options in short form are prefixed with one '-' and are one character long, while options in long form are prefixed
with two '-'s and are two or more characters long. If an option requires a value, next argument on the command-line is taken to be its value.
.P
Only ASCII is supported.
.SH DATA STRUCTURES
All data structures are defined in
.I marg/marg_const.h
file.
This file is included by
.I marg/marg.h
file so there is no need to include it separately.
.SS MargSuccess
This data structure is an enum that contains all codes returned by the functions of this library.
Those return codes and their descriptions are laid out in the following table.
.TS
allbox;
cb | cb
c | l.
CODE	DESCRIPTION
MARG_SUCCESS_SUCCESS	No errors.
MARG_SUCCESS_NO_MEMORY	Failed to allocate memory.
MARG_SUCCESS_INVALID_ARG	Passed command-line is invalid (parsing).
MARG_SUCCESS_EXTRA_ARG	Success, but user specified additional argument (parsing).
.TE
.SS MargArg
This data structure is a struct that contains argument definition, and it consists of five variables.
.P
First three of these fields are set by the programmer using
.BR marg_set_arg ()
function.
.I short_form
represents short form of the argument and is one character long.
If it is set to '\\0', short form of the argument will be invisible during parsing.
.I long_form
should be set to a string of two or more characters and it represents the long form of the argument.
Setting it to NULL or to a string less than two characters long will cause it to be invisible during parsing.
.I takes_a_value
should be set if the argument requires a value (as the next argument).
.P
Last two fields are by default set to false and NULL by
.BR marg_set_arg (),
respectively, but
.BR marg_parse_args ()
may modify them during the parsing stage:
.I activated
will be set if the argument whose either short or long (or both) form is given as a command-line argument.
If that argument expects a value to be passed,
.I value
field will point at its value (next passed argument).
.SH USAGE
Using
.B marg
is divided into five steps, with each step having its corresponding function.
Here those steps are described in the order in which they should be performed.
.SS Initialization
Function dedicated to this step is
.BR marg_init ().
The job of this function is to prepare everything for defining and parsing command-line arguments.
What this function actually does is that it creates on the heap an array able to hold exactly
.I count
pointers to
.B struct MargArg
data structures.
Note that passing 0 as
.I count
argument doesn't actually do anything and is equivalent to not calling the function at all.
.P
This function should be called only once.
.SS Definitions
Use
.BR marg_set_arg ()
to set exactly as many arguments as for how many arguments you initialized the parser in the previous step (i.e. call this function exactly
.I count
times to define
.I count
arguments).
This function will create new
.B struct MargArg
data structure on the heap and will set its
.IR short_form ,
.I long_form
(which is copied) and
.I takes_a_value
fields to the corresponding function arguments.
In case you don't want an argument to have a form (e.g. you want only '--help' but not '-h') set the corresponding argument form parameter of
.BR marg_set_arg ()
to '\\0' or NULL, as applicable (notice that setting both argument forms to NULL will create
.B struct MargArg
data structure on the heap but the argument itself will not ever match any command-line argument).
Other two fields
.RI "(" "activated" " and " "value" ")"
will be set to false and NULL, respectively.
.P
Pointer to the newly created data structure will be appended to the argument array created in the previous step, without checking for overflow.
.SS Parsing
This step is the main step of the library.
Associated function is
.BR marg_parse_args ().
It takes three arguments: a pointer to
.IR argc ,
.I argv
and
.I extra
(a boolean value).
.I argc
should point to the argument count from
.BR main (),
and analogically
.IR argv
should be argument vector from
.BR main ().
.P
This function will scan the argument vector for options (everything that starts with '-' is considered an option) defined in the previous step.
If
.I argv
contains an invalid argument this function will return with
.B MARG_SUCCESS_INVALID_ARG
code.
Argument will be considered invalid if:
.RS
.IP \(bu
It contains less than 2 characters.
.IP \(bu
It does not start with '-'.
.IP \(bu
It starts with '--' but the actual argument (the part of the argument after starting '--') has less than 2 characters.
.IP \(bu
It accepts value but the value is not given i.e. it is the last argument or it is given in short form as combined argument.
.IP \(bu
It does not match any of the arguments defined in the previous step.
.RE
.P
The scanning itself is done by comparing each given argument against
.I short_form
and
.I long_form
of each defined argument in the array of
.B struct MargArg
pointers, depending on whether the given argument itself starts with one or two '-'s, respectively.
In the former case, comparison is done character by character, and in the latter case the entire argument needs to match.
.P
If a match is found,
.I activated
field of the corresponding
.B struct MargArg
data structure is set to true and if its
.I takes_a_value
field is set, next argument is blindly copied on the heap and pointer to that copy is stored into this object's
.I value
field.
An argument that is considered to be an option value is not parsed.
.P
Lastly, you may wish to allow user to specify to your program an input file, an output file, or whatever kind of additional argument, which is not associated with an option.
For this you should use
.I extra
parameter.
When
.I extra
is set, if the last argument does not match any of the defined arguments (which does not mean it is an undefined argument), error will not be produced and the function
will return with
.B MARG_SUCCESS_EXTRA_ARG
code to signify that additional argument has been passed.
.P
Call this function only once.
.SS Retrieval
After succesful parsing retrieve the defined argument structures.
For this
.BR marg_arg_get ()
is provided.
.BR marg_arg_get ()
will return pointer to the
.IR "n" "th"
.B struct MargArg
data structure in the collection of defined arguments (remember that data structures are appended by
.BR marg_set_arg ()
so they are stored in the order in which they were defined, starting from 0).
Use returned pointer of the requested data structure to inspect its
.I activated
and
.I value
fields to find out whether the argument was given and, if it requires a value, what was passed as its value.
.P
The library does not check whether
.I n
is in the defined range of arguments.
.SS Closing
Parser should be closed when you are done with parsing.
.BR marg_close ()
will destroy all data stored on the heap in the defined arguments collection.
Note that after call to this function all
.B struct MargArg
data structures you retrieved in the previous step don't exist anymore.
.P
Call it only once.
.SH RETURN VALUE
.TP
.BR marg_init ()
On success
.B MARG_SUCCESS_SUCCESS
is returned.
If there was not enough memory,
.B MARG_SUCCESS_NO_MEMORY
is returned.
.TP
.BR marg_set_arg ()
On success
.B MARG_SUCCESS_SUCCESS
is returned.
If there was not enough memory,
.B MARG_SUCCESS_NO_MEMORY
is returned.
.TP
.BR marg_parse_args ()
In case there was not enough memory
.B MARG_SUCCESS_NO_MEMORY
is returned.
If there was a parsing error,
.B MARG_SUCCESS_INVALID_ARG
is returned.
If there were no errors and additional argument is not found,
.B MARG_SUCCESS_SUCCESS
is returned.
If there were no errors and additional argument is found,
.B MARG_SUCCESS_EXTRA_ARG
is returned.
.TP
.BR marg_get_arg ()
Returns pointer to the requested
.B struct MargArg
data structure.
.SH NOTES
This library is meant to be used strictly according to the algorithm laid out in section
.BR USAGE .
You should not attempt to parse multiple command-lines without shutting down the previous parser.
.P
In a case that any, except for
.BR marg_init (),
of the functions returns a failure code, you should call
.BR marg_close ()
to free the created data structures.
Then you may try again.
