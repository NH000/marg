/*
 * Copyright (C) Nikola Hadžić 2019-2021
 *
 * This file is part of marg.
 *
 * marg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * marg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with marg.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MARG_H
#define MARG_H
#include <stdbool.h>
#include "marg_const.h"

enum MargSuccess marg_init(const unsigned int count);
enum MargSuccess marg_set_arg(const char short_form, const char * const long_form, const bool takes_a_value);
enum MargSuccess marg_parse_args(const int * const argc, const char * const * const argv, const bool extra);
const struct MargArg *marg_get_arg(const unsigned int n);
void marg_close();

#endif
