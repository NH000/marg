/*
 * Copyright (C) Nikola Hadžić 2019-2021
 *
 * This file is part of marg.
 *
 * marg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * marg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with marg.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MARG_CONST_H
#define MARG_CONST_H
#include <stdbool.h>

// Success codes.
enum MargSuccess {
            MARG_SUCCESS_SUCCESS,       // No errors.
            MARG_SUCCESS_NO_MEMORY,     // Couldn't allocate enough memory.
            MARG_SUCCESS_INVALID_ARG,   // User supplied invalid argument.
            MARG_SUCCESS_EXTRA_ARG          // Additional argument supplied.
        };

// Stores an argument.
struct MargArg
{
    char short_form;    // Short form of the argument, e.g. '-a'.
    char *long_form;    // Long form of the argument, e.g. '--argument'.
    bool takes_a_value; // Whether an argument accepts a value.
    bool activated;     // Whether the argument was given.
    char *value;        // If the argument takes a value, this is the value.
};

#endif
