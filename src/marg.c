/*
 * Copyright (C) Nikola Hadžić 2019-2021
 *
 * This file is part of marg.
 *
 * marg is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * marg is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with marg.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "marg_const.h"

// Collection of defined command-line arguments.
static struct MargArg **_marg_arg_collection;
static unsigned int _marg_arg_collection_size;

// Initializes argument parser with the number of distinct argument
// types passed as `count` parameter.
enum MargSuccess marg_init(const unsigned int count)
{
    // Allocate argument collection for total number of command-line arguments.
    if (!(_marg_arg_collection = calloc(count, sizeof(struct MargArg *))))
        return MARG_SUCCESS_NO_MEMORY;

    // Save argument collection size.
    _marg_arg_collection_size = count;

    return MARG_SUCCESS_SUCCESS;
}

// Defines new argument to accept.
// Stores short form, long form and whether an argument takes a value.
// `long_form` is copied.
enum MargSuccess marg_set_arg(const char short_form, const char * const long_form, const bool takes_a_value)
{
    // Holds serial number of the argument which is being initialized.
    static unsigned int current_arg = 0;

    // Create new argument holder.
    struct MargArg * const marg_arg = malloc(sizeof(struct MargArg));
    if (!marg_arg)
        return MARG_SUCCESS_NO_MEMORY;

    // Store argument values.
    marg_arg->short_form = short_form;
    if (long_form && !(marg_arg->long_form = strdup(long_form)))
        return MARG_SUCCESS_NO_MEMORY;
    marg_arg->takes_a_value = takes_a_value;
    marg_arg->activated = false;
    marg_arg->value = NULL;

    // Store the argument into the collection of arguments.
    _marg_arg_collection[current_arg++] = marg_arg;

    return MARG_SUCCESS_SUCCESS;
}

// Parses given command-line arguments.
// If `extra` is set last argument shall not produce an error if given invalid, but will be considered to be extra argument.
enum MargSuccess marg_parse_args(const int * const argc, const char * const * const argv, const bool extra)
{
    for (int i = 1;i < *argc;i++)
    {
        // Get the current argument.
        const char * const current_arg = argv[i];
        const size_t current_arg_length = strlen(current_arg);

        // Check for argument validity.
        if (current_arg_length < 2 || current_arg[0] != '-' || (current_arg_length < 4 && current_arg[1] == '-'))
        {
            if (extra && i == *argc - 1)
                return MARG_SUCCESS_EXTRA_ARG;
            else
                return MARG_SUCCESS_INVALID_ARG;
        }

        // Parse the argument.
        if (current_arg[1] == '-')  // Long form.
        {
            // Get the actual argument for comparison.
            const char * const actual_arg = current_arg + 2;
            bool success = false;

            // Find argument that matches in the collection of defined arguments.
            for (unsigned int j = 0;j < _marg_arg_collection_size;j++)
            {
                // An argument matches, set activated flag.
                if (_marg_arg_collection[j]->long_form != NULL && !strcmp(_marg_arg_collection[j]->long_form, actual_arg))
                {
                    _marg_arg_collection[j]->activated = true;
                    success = true;

                    // Store the next argument as the value if argument takes a value.
                    if (_marg_arg_collection[j]->takes_a_value)
                    {
                        // Return error if this argument is the last one.
                        if (i == *argc - 1)
                            return MARG_SUCCESS_INVALID_ARG;
                        else if (!(_marg_arg_collection[j]->value = strdup(argv[++i])))
                            return MARG_SUCCESS_NO_MEMORY;
                    }
                    break;
                }
            }

            if (!success)
                return MARG_SUCCESS_INVALID_ARG;
        }
        else                // Short form.
        {
            // Check each letter of the flag in the short form.
            for (size_t j = 1;j < current_arg_length;j++)
            {
                bool success = false;

                // Test every letter against short form of every specified argument.
                for (unsigned int k = 0;k < _marg_arg_collection_size;k++)
                {
                    // If an argument matches mark it as activated.
                    if (_marg_arg_collection[k]->short_form != '\0' && _marg_arg_collection[k]->short_form == current_arg[j])
                    {
                        _marg_arg_collection[k]->activated = true;
                        success = true;

                        // Test if an argument takes a value and if it does return an error if flag is combined with other flags
                        // or it is the last argument, otherwise store the next argument as the value.
                        if (_marg_arg_collection[k]->takes_a_value)
                        {
                            if (current_arg_length > 2 || i == *argc - 1)
                                return MARG_SUCCESS_INVALID_ARG;
                            else
                            {
                                if (!(_marg_arg_collection[k]->value = strdup(argv[++i])))
                                    return MARG_SUCCESS_NO_MEMORY;
                            }
                        }
                        break;
                    }
                }

                if (!success)
                    return MARG_SUCCESS_INVALID_ARG;
            }
        }
    }

    return MARG_SUCCESS_SUCCESS;
}

// Returns nth argument. Use after parsing.
const struct MargArg *marg_get_arg(const unsigned int n)
{
    return _marg_arg_collection[n];
}

// Releases the memory and closes the parser.
void marg_close()
{
    for (int i = 0;i < _marg_arg_collection_size;i++)
    {
        free(_marg_arg_collection[i]->value);
        free(_marg_arg_collection[i]);
    }

    free(_marg_arg_collection);
}
