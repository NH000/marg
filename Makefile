# INSTALL COMMANDS
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# USER MODIFIABLE VARIABLES
LIBRARY := dynamic
PROG := marg
DEBUG :=
PROFILE :=
OPTIMIZE :=
CFLAGS := -Wall -c
LDFLAGS :=
ARFLAGS := -rcs
SRCDIR := src
DEPSDIR := .deps
OBJDIR := obj
MANDIR := man
LIBDEST := /usr/lib
HEADDEST := /usr/include/$(PROG)
MANDEST := /usr/share/man

# USER NON MODIFIABLE VARIABLES
SRCS := $(wildcard $(SRCDIR)/*.c)
HDRS := $(wildcard $(SRCDIR)/*.h)
OBJS := $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCS))

# LIBNAME
ifeq ($(LIBRARY), dynamic)
    LIBNAME := lib$(PROG).so
else
ifeq ($(LIBRARY), static)
    LIBNAME := lib$(PROG).a
else
$(error Invalid LIBRARY type)
endif
endif

# ENABLE DEBUG
ifdef DEBUG
    CFLAGS := $(CFLAGS) -g
endif

# ENABLE PROFILE
ifdef PROFILE
    CFLAGS := $(CFLAGS) -pg
    LDFLAGS := $(LDFLAGS) -pg
endif

# ENABLE OPTIMIZE
ifdef OPTIMIZE
    CFLAGS := $(CFLAGS) -O3
endif

# FUNCTIONS

# Recursively installs manual pages to subdirectory $(1) of $(MANDEST).
install_man = $(foreach f,$(notdir $(wildcard $(MANDIR)/$(1)/*)),$(if $(shell test -d '$(MANDIR)/$(1)/$(f)' && echo "dir"),$(call install_man,$(1)/$(f)),$(INSTALL_DATA) -D 'man/$(1)/$(f)' '$(MANDEST)/$(1)/$(PROG).3';))

# Recursively removes installed manual pages from subdirectory $(1) of $(MANDEST).
uninstall_man = $(foreach f,$(notdir $(wildcard $(MANDIR)/$(1)/*)),$(if $(shell test -d '$(MANDIR)/$(1)/$(f)' && echo "dir"),$(call uninstall_man,$(1)/$(f)),if test -d '$(MANDEST)/$(1)'; then rm -f '$(MANDEST)/$(1)/$(PROG).3' && rmdir -p --ignore-fail-on-non-empty '$(MANDEST)/$(1)'; fi;))

# DEPENDENCIES
include $(wildcard $(DEPSDIR)/*.d)

# PHONY TARGETS
.PHONY: help deps lib clean install-lib install-headers install-man uninstall-lib uninstall-headers uninstall-man

# HELP MENU
help:
	$(info ===VARIABLES===)
	$(info LIBRARY:  Library type.)
	$(info PROG:     Name of the generated library.)
	$(info DEBUG:    Embed debugging information.)
	$(info PROFILE:  Embed profiling information.)
	$(info OPTIMIZE: Produce optimized binaries.)
	$(info CC:       Compiler to use.)
	$(info CFLAGS:   Compiler flags.)
	$(info LDFLAGS:  Linker flags.)
	$(info AR:       Archiver to use.)
	$(info ARFLAGS:  Archiver flags.)
	$(info SRCDIR:   Source directory.)
	$(info DEPSDIR:  Dependency information directory.)
	$(info OBJDIR:   Object directory.)
	$(info MANDIR:   Manual directory.)
	$(info LIBDEST:  Library destination directory.)
	$(info HEADDEST: Header destination directory.)
	$(info MANDEST:  Manual destination directory.)
	$(info )
	$(info ===RULES===)
	$(info help:              Display this help menu.)
	$(info deps:              Build dependecy information.)
	$(info lib:               Build specified library.)
	$(info clean:             Delete produced files.)
	$(info install-lib:       Install specified library.)
	$(info install-headers:   Install library headers.)
	$(info install-man:       Install manual pages.)
	$(info uninstall-lib:     Uninstall specified library.)
	$(info uninstall-headers: Uninstall headers.)
	$(info uninstall-man:     Uninstall manual pages.)

# Directory creator.
$(OBJDIR) $(DEPSDIR):
	@mkdir -p '$@'

deps: $(SRCS) | $(DEPSDIR)
	$(info Building dependencies...)
	@$(foreach src,$(SRCS),$(CC) -MT $(patsubst $(SRCDIR)/%.c,'$(OBJDIR)/%.o',$(src)) -MM -MF $(patsubst $(SRCDIR)/%.c,'$(DEPSDIR)/%.d',$(src)) '$(src)')

lib: $(LIBNAME)

$(LIBNAME): $(OBJS)
	$(if $(findstring dynamic,$(LIBRARY)),$(CC) $(LDFLAGS) -shared -o '$@' $(patsubst %,'%',$^),$(AR) $(ARFLAGS) '$@' $(patsubst %,'%',$^))

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	$(CC) $(CFLAGS) $(if $(findstring dynamic,$(LIBRARY)),-fPIC) -o '$@' '$<'

clean:
	$(info Cleaning directory....)
	@rm -rf '$(OBJDIR)' '$(DEPSDIR)' 'lib$(PROG).so' 'lib$(PROG).a'

install-lib: $(LIBNAME)
	$(if $(findstring dynamic,$(LIBRARY)),$(INSTALL_PROGRAM),$(INSTALL_DATA)) -D -t '$(LIBDEST)' '$(LIBNAME)'

install-headers: $(HDRS)
	$(INSTALL_DATA) -D -t '$(HEADDEST)' $(patsubst %,'%',$(HDRS))

install-man: $(MANDIR)
	$(call install_man)

uninstall-lib:
	rm -f '$(LIBDEST)/$(LIBNAME)'

uninstall-headers:
	rm -rf '$(HEADDEST)'

uninstall-man:
	$(call uninstall_man)
